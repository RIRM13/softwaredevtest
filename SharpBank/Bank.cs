﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SharpBank.Bank"/> class.
        /// </summary>
        public Bank()
        {
            customers = new List<Customer>();
        }

        /// <summary>
        /// Adds the customer.
        /// </summary>
        /// <param name="customer">Customer.</param>
        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        /// <summary>
        /// Customers the summary.
        /// </summary>
        /// <returns>The summary.</returns>
        public String CustomerSummary()
        {
            String summary = "Customer Summary";
            foreach (Customer c in customers)
                summary += "\n - " + c.GetName() + " (" + Format(c.GetNumberOfAccounts(), "account") + ")";
            return summary;
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        /// <summary>
        /// Format the specified number and word.
        /// </summary>
        /// <returns>The format.</returns>
        /// <param name="number">Number.</param>
        /// <param name="word">Word.</param>
        private String Format(int number, String word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public double TotalInterestPaid()
        {
            double total = 0;
            foreach (Customer c in customers)
                total += c.TotalInterestEarned();
            return total;
        }

        /// <summary>
        /// Gets the first customer.
        /// </summary>
        /// <returns>The first customer.</returns>
        public String GetFirstCustomer()
        {
            try
            {
                customers = null;
                return customers[0].GetName();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "Error";
            }
        }    

    }
}
