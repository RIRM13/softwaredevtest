﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        /// <summary>
        /// The transaction date.
        /// </summary>
        public readonly DateTime transactionDate;
        public readonly string operationType;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SharpBank.Transaction"/> class.
        /// </summary>
        /// <param name="amount">Amount.</param>
        public Transaction(double amount)
        {
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SharpBank.Transaction"/> class.
        /// </summary>
        /// <param name="amount">Amount.</param>
        /// <param name="action">Action.</param>
        public Transaction(double amount, string action)
        {
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
            this.operationType = action;
        }
       
    }
}
