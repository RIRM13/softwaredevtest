﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;
        OperationResult result = new OperationResult();

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SharpBank.Customer"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <returns>The name.</returns>
        public String GetName()
        {
            return name;
        }

        /// <summary>
        /// Opens the account.
        /// </summary>
        /// <returns>The account.</returns>
        /// <param name="account">Account.</param>
        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        /// <summary>
        /// Gets the number of accounts.
        /// </summary>
        /// <returns>The number of accounts.</returns>
        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        /// <summary>
        /// Totals the interest earned.
        /// </summary>
        /// <returns>The interest earned.</returns>
        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        /// <summary>
        /// Gets the statement.
        /// </summary>
        /// <returns>The statement.</returns>
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        /// <summary>
        /// Statements for account.
        /// </summary>
        /// <returns>The for account.</returns>
        /// <param name="a">The alpha component.</param>
        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case Account.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        /// <summary>
        /// Convert an amount to dollars.
        /// </summary>
        /// <returns>The dollars.</returns>
        /// <param name="d">D.</param>
        private String ToDollars(double d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }

        /// <summary>
        /// Transactions the between accounts.
        /// </summary>
        /// <returns>The between accounts.</returns>
        /// <param name="accountTypeOrigin">Account type origin.</param>
        /// <param name="accountTypeToTransfer">Account type to transfer.</param>
        /// <param name="amount">Amount.</param>
        public OperationResult TransactionBetweenAccounts(Account accountOrigin, Account accountToTransfer, double amount)
        {
            try
            {
                if (accountOrigin.GetAccountType() != accountToTransfer.GetAccountType()) // Comparing the account type - Transfer cannot be available for the same account type for one client
                {
                    if (accountOrigin.GetAccountBalance() >= amount) // Is the amount available
                    {
                        accountOrigin.Withdraw(amount);     // Withdraw the amount from the origin account 
                        accountToTransfer.Deposit(amount);  // Deposit the amount in the destination account

                        result.OperationCode = 200;
                        result.Result = "Transacction completed successfully.";
                    }
                    else
                    {
                        result.OperationCode = 100;
                        result.Result = "This account does not have this amount to complete the transaction.";
                    }
                }
                else
                {
                    result.OperationCode = 300;
                    result.Result = "The accounts have to be different";
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Account - Transfer Exception 01: {0}", ex);
            }
        }
    }
}
