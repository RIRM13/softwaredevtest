﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {

        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        public double AccountBalance { get; set; }

        private readonly int accountType;
        public List<Transaction> transactions;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SharpBank.Account"/> class.
        /// </summary>
        /// <param name="accountType">Account type.</param>
        public Account(int accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        /// <summary>
        /// Deposit the specified amount.
        /// </summary>
        /// <returns>The deposit.</returns>
        /// <param name="amount">Amount.</param>
        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                AccountBalance += amount;
                transactions.Add(new Transaction(amount, "Deposit"));
            }
        }

        /// <summary>
        /// Withdraw the specified amount.
        /// </summary>
        /// <returns>The withdraw.</returns>
        /// <param name="amount">Amount.</param>
        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                AccountBalance -= amount; //Substracting the 
                transactions.Add(new Transaction(-amount, "Withdraw"));
            }
        }

        /// <summary>
        /// Interests the earned.
        /// </summary>
        /// <returns>The earned.</returns>
        public double InterestEarned()
        {
            double amount = SumTransactions();
            switch (accountType)
            {
                case SAVINGS:
                    if (amount <= 1000)
                        return amount * 0.001;
                    else
                        return 1 + (amount - 1000) * 0.002;
               case MAXI_SAVINGS:
                     
                    if (GetDayDiffLastOperationByType() > 10)
                        return amount * 0.005;
                    else
                        return amount * 0.001;                    
                default:
                    return amount * 0.001;
            }
        }

        /// <summary>
        /// Sums the transactions.
        /// </summary>
        /// <returns>The transactions.</returns>
        public double SumTransactions()
        {
            return CheckIfTransactionsExist(true);
        }

        /// <summary>
        /// Checks if transactions exist.
        /// </summary>
        /// <returns>The if transactions exist.</returns>
        /// <param name="checkAll">If set to <c>true</c> check all.</param>
        private double CheckIfTransactionsExist(bool checkAll)
        {
            double amount = 0.0;
            foreach (Transaction t in transactions)
                amount += t.amount;
            return amount;
        }

        /// <summary>
        /// Gets the type of the account.
        /// </summary>
        /// <returns>The account type.</returns>
        public int GetAccountType()
        {
            return accountType;
        }

        /// <summary>
        /// Gets the account balance.
        /// </summary>
        /// <returns>The account balance.</returns>
        public double GetAccountBalance(){
            return AccountBalance;
        }

        /// <summary>
        /// Gets the type of the day diff last operation by.
        /// </summary>
        /// <returns>The day diff last operation by type.</returns>
        public double GetDayDiffLastOperationByType()
        {
            var lastDate = (from val in transactions
                            where val.operationType == "Withdraw"
                           select new {
                valTransaction = val.amount,
                date = val.transactionDate}).OrderByDescending(i=>i.date) .FirstOrDefault();

            if (lastDate == null){
                return 20;
            }
            else
            {
                return (lastDate.date - DateTime.Now).TotalDays;    
            }


        }
      
    }
}
