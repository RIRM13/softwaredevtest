﻿using System;
namespace SharpBank
{
    public class OperationResult
    {

        public int OperationCode { get; set; }
        public string Result { get; set; }
    }
}
