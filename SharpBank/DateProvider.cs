﻿using System;

namespace SharpBank
{
    public class DateProvider
    {
        private static DateProvider instance = null;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <returns>The instance.</returns>
        public static DateProvider GetInstance()
        {
            if (instance == null)
                instance = new DateProvider();
            return instance;
        }

        /// <summary>
        /// Now this instance, return the actual date and time
        /// </summary>
        /// <returns>The now.</returns>
        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}
